import React from 'react';
import FullWidthTabs from './TabPanel';
import {Nav} from './style'

//Navbar renders tabs used from survey data
export default function NavBar(props) {
    const {data} = props;
  
    return (
     <Nav>
        <h1>Surveys</h1>
        <FullWidthTabs tabData = {data.data.surveys}/>


        </Nav>
    );
  }