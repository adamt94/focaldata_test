import React from 'react';
import { render } from '@testing-library/react';
import {fetchSurveyData} from '../App';
import TabPanel from './TabPanel'
import renderer from 'react-test-renderer';


const mockData = {
  "surveys": [
    {
      "surveyId": 1,
      "title": "Brexit survey",
      "questions": [
        {
          "questionId": 1,
          "questionTitle": "I want Britain to leave the EU.",
          "answerOptions": [
            {
              "answerOption": 1,
              "text": "Strongly disagree",
              "selectedByRespondents": 30
            },
            {
              "answerOption": 2,
              "text": "Disagree",
              "selectedByRespondents": 10
            },
            {
              "answerOption": 3,
              "text": "Neither agree nor disagree",
              "selectedByRespondents": 20
            },
            {
              "answerOption": 4,
              "text": "Agree",
              "selectedByRespondents": 10
            },
            {
              "answerOption": 5,
              "text": "Strongly agree",
              "selectedByRespondents": 30
            }
          ]
        },
        {
          "questionId": 2,
          "questionTitle": "I expect another Brexit referendum.",
          "answerOptions": [
            {
              "answerOption": 1,
              "text": "Strongly disagree",
              "selectedByRespondents": 40
            },
            {
              "answerOption": 2,
              "text": "Disagree",
              "selectedByRespondents": 10
            },
            {
              "answerOption": 3,
              "text": "Neither agree nor disagree",
              "selectedByRespondents": 20
            },
            {
              "answerOption": 4,
              "text": "Agree",
              "selectedByRespondents": 10
            },
            {
              "answerOption": 5,
              "text": "Strongly agree",
              "selectedByRespondents": 20
            }
          ]
        },
        {
          "questionId": 3,
          "questionTitle": "I'm happy that Boris Johnson is the UK Prime Minister.",
          "answerOptions": [
            {
              "answerOption": 1,
              "text": "Strongly disagree",
              "selectedByRespondents": 40
            },
            {
              "answerOption": 2,
              "text": "Disagree",
              "selectedByRespondents": 10
            },
            {
              "answerOption": 3,
              "text": "Neither agree nor disagree",
              "selectedByRespondents": 15
            },
            {
              "answerOption": 4,
              "text": "Agree",
              "selectedByRespondents": 15
            },
            {
              "answerOption": 5,
              "text": "Strongly agree",
              "selectedByRespondents": 20
            }
          ]
        },
        {
          "questionId": 4,
          "questionTitle": "Leaving the EU will improve the British economy.",
          "answerOptions": [
            {
              "answerOption": 1,
              "text": "Strongly disagree",
              "selectedByRespondents": 40
            },
            {
              "answerOption": 2,
              "text": "Disagree",
              "selectedByRespondents": 10
            },
            {
              "answerOption": 3,
              "text": "Neither agree nor disagree",
              "selectedByRespondents": 20
            },
            {
              "answerOption": 4,
              "text": "Agree",
              "selectedByRespondents": 10
            },
            {
              "answerOption": 5,
              "text": "Strongly agree",
              "selectedByRespondents": 20
            }
          ]
        }
      ]
    }
  ]
}


it('Overview Component renders correctly', () => {
  const tree = renderer
    .create(<TabPanel data ={mockData.surveys}/>)
    .toJSON();
  expect(tree).toMatchSnapshot();
});

