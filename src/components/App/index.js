import React from 'react';
import './App.css';
import fetchData from '../../util/FetchData';
import { MuiThemeProvider } from '@material-ui/core/styles';
import {theme} from '../../util/colors'


import NavBar from '../NavBar';
const POSTS_SERVICE_URL = 'https://my-json-server.typicode.com/focaldata/demo/db';



function App() {
  return (
    <MuiThemeProvider theme={theme}>
      <NavBar data={fetchSurveyData()}/>
    </MuiThemeProvider>
  );
}

export function fetchSurveyData() {
  const dataState = fetchData(POSTS_SERVICE_URL);
  console.log(dataState)
  return dataState;
}

export default App;
