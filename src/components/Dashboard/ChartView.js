import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import {Wrapper} from './style'
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import {primary, graphColors} from '../../util/colors'
import {Doughnut, Bar} from 'react-chartjs-2';
import PropTypes from 'prop-types';



const useStyles = makeStyles({
    root: {
      maxWidth: 775,
    },
  });

export default function ChartView(props) {
    const{ questionTitle } = props.survey;
    const{type} = props;
    const classes = useStyles();
    
  
    return (
    <Wrapper>
    <Card className={classes.root}>
      <CardActionArea>
        <CardContent>
          <Typography gutterBottom color="primary" variant="h6" component="h2">
              {questionTitle}
            </Typography>
            {type === "bar" ?( 
              <Bar data={convertDataToChartData(props.survey)} options={{
                scales: {
                  yAxes: [{
                    ticks: {
                      beginAtZero: true
                    }
                  }]
                }
              }}  />
          ): (  <Doughnut data={convertDataToChartData(props.survey)} />)
          }
            
          </CardContent>
      </CardActionArea>
    </Card>
    </Wrapper>


    );




     //converts data to work with chartjs format     
    function convertDataToChartData(data){
        let values = [];
        let textLabels = []
        data.answerOptions.forEach(element => {
            values.push(element.selectedByRespondents);
            textLabels.push(element.text);
        });
        const chartData = {
            labels: textLabels,
            datasets: [
              {
                label: 'My First dataset',
                backgroundColor: graphColors,
                borderWidth: 1,
                hoverBackgroundColor: 'rgba(255,99,132,0.4)',
                hoverBorderColor: 'rgba(255,99,132,1)',
                data: values
              }
            ]
          }
          return chartData;
    }
  }


  ChartView.propTypes = {
    survey: PropTypes.object
  }

