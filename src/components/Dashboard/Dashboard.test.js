import React from 'react';
import Overview from './Overview'
import renderer from 'react-test-renderer';
import ChartView from './ChartView';

const mockData = {
  "surveys": [
    {
      "surveyId": 1,
      "title": "Brexit survey",
      "questions": [
        {
          "questionId": 1,
          "questionTitle": "I want Britain to leave the EU.",
          "answerOptions": [
            {
              "answerOption": 1,
              "text": "Strongly disagree",
              "selectedByRespondents": 30
            },
            {
              "answerOption": 2,
              "text": "Disagree",
              "selectedByRespondents": 10
            },
            {
              "answerOption": 3,
              "text": "Neither agree nor disagree",
              "selectedByRespondents": 20
            },
            {
              "answerOption": 4,
              "text": "Agree",
              "selectedByRespondents": 10
            },
            {
              "answerOption": 5,
              "text": "Strongly agree",
              "selectedByRespondents": 30
            }
          ]
        },
        {
          "questionId": 2,
          "questionTitle": "I expect another Brexit referendum.",
          "answerOptions": [
            {
              "answerOption": 1,
              "text": "Strongly disagree",
              "selectedByRespondents": 40
            },
            {
              "answerOption": 2,
              "text": "Disagree",
              "selectedByRespondents": 10
            },
            {
              "answerOption": 3,
              "text": "Neither agree nor disagree",
              "selectedByRespondents": 20
            },
            {
              "answerOption": 4,
              "text": "Agree",
              "selectedByRespondents": 10
            },
            {
              "answerOption": 5,
              "text": "Strongly agree",
              "selectedByRespondents": 20
            }
          ]
        },
        {
          "questionId": 3,
          "questionTitle": "I'm happy that Boris Johnson is the UK Prime Minister.",
          "answerOptions": [
            {
              "answerOption": 1,
              "text": "Strongly disagree",
              "selectedByRespondents": 40
            },
            {
              "answerOption": 2,
              "text": "Disagree",
              "selectedByRespondents": 10
            },
            {
              "answerOption": 3,
              "text": "Neither agree nor disagree",
              "selectedByRespondents": 15
            },
            {
              "answerOption": 4,
              "text": "Agree",
              "selectedByRespondents": 15
            },
            {
              "answerOption": 5,
              "text": "Strongly agree",
              "selectedByRespondents": 20
            }
          ]
        },
        {
          "questionId": 4,
          "questionTitle": "Leaving the EU will improve the British economy.",
          "answerOptions": [
            {
              "answerOption": 1,
              "text": "Strongly disagree",
              "selectedByRespondents": 40
            },
            {
              "answerOption": 2,
              "text": "Disagree",
              "selectedByRespondents": 10
            },
            {
              "answerOption": 3,
              "text": "Neither agree nor disagree",
              "selectedByRespondents": 20
            },
            {
              "answerOption": 4,
              "text": "Agree",
              "selectedByRespondents": 10
            },
            {
              "answerOption": 5,
              "text": "Strongly agree",
              "selectedByRespondents": 20
            }
          ]
        }
      ]
    }
  ]
}
jest.mock('react-chartjs-2', () => ({
  Bar: () => null
}));

it('Overview Component renders correctly', () => {
  const tree = renderer
    .create(<Overview survey ={mockData.surveys[0]} type="bar"/>)
    .toJSON();
  expect(tree).toMatchSnapshot();
});


it('ChartView Component renders correctly', () => {

  console.log(mockData.surveys[0].questions[0])
  const test = renderer
    .create(<ChartView survey ={mockData.surveys[0].questions[0]} type="bar"/>)
    .toJSON();
  expect(test).toMatchSnapshot();
});