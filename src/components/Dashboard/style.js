import styled from 'styled-components'
import { secondary} from '../../util/colors'

export const Wrapper = styled.section`
padding: 1rem;
color: ${secondary}
`;


export const Content = styled.section`
padding: 2.2rem;
`;

export const GridLayout = styled.section`
display: grid;
grid-template-columns: repeat(auto-fill, minmax(500px, 1fr));
`;

export const CardButtons = styled.section`
float: right;
`;