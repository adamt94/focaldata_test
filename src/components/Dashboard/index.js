import React,{ useState } from 'react';
import Overview from './Overview'
import ChartView from './ChartView'
import {GridLayout} from './style'


//Nav Bar tabs is created from survey data
export default function NavBar(props) {
    const { survey } = props;
    const {title } = survey;
    const [chartType, setChartType] = useState('bar');
    console.log(title)

    const handleClick = (type) =>{
      
      setChartType(type);
      console.log(chartType)

    }
  
    return (
    <div>
      <Overview survey = {survey} handleClick={handleClick}/>
      <GridLayout>
        {survey.questions.map(row => (
            
              <ChartView survey = {row} type = {chartType}/>
          
            ))}
      </GridLayout>
   
    </div>



    );
  }
