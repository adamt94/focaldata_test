import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import {Wrapper,CardButtons} from './style'
import EqualizerIcon from '@material-ui/icons/Equalizer';
import IconButton from '@material-ui/core/IconButton';
import PieChartIcon from '@material-ui/icons/PieChart';



const useStyles = makeStyles({
    root: {
      width: 345,
    },
  });

export default function Overview(props) {
  console.log(props)
  const { title } = props.survey;
  const classes = useStyles();
  const {total_questions,total_votes} = getData(props.survey);
   

    return (
    
    <Wrapper>
      <Card className={classes.root}>
        <CardActionArea>
          <CardContent>
            <Typography gutterBottom color="primary" variant="h5" component="h2">
              {title}
            </Typography>
            <Typography variant="body1" color="textSecondary" component="p">
              Question Count: 
            </Typography>
            <Typography variant="body1" color="primary" component="p">
              {total_questions}
            </Typography>
            <Typography variant="body1" color="textSecondary" component="p">
              Total Responses: 
            </Typography>
            <Typography variant="body1" color="primary" component="p">
              {total_votes}
            </Typography>

            <CardButtons>
              <IconButton color="primary" aria-label="upload picture" component="span" onClick={() => props.handleClick('bar')}>
                <EqualizerIcon color="primary"/>
              </IconButton>
              <IconButton color="primary" aria-label="upload picture" component="span" onClick={() => props.handleClick('pie')}>
                <PieChartIcon color="primary"/>
              </IconButton>
            </CardButtons>
            
          </CardContent>
        </CardActionArea>
      </Card>
    </Wrapper>


    );

}


//calculates overview data from survey data
function getData(survey){
    let total_votes  = 0;
    console.log(survey.questions)
    const total_questions = survey.questions.length;
    survey.questions.forEach(element => {
        
        element.answerOptions.forEach(item => {
            total_votes += item.selectedByRespondents
            console.log(item)
        });
       
    });

    return {total_questions,total_votes}
}