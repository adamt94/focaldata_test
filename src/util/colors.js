import { createMuiTheme } from '@material-ui/core/styles';


export const primary = "#009be5";
export const secondary = "#ffffff";
export const background = "#eaeff1";
export const graphColors = ["#009be5","#4caaea","#70b9ef","#8ec8f4","#abd7fa" ]


//material ui theme
export const theme = createMuiTheme({
    palette: {
      type: 'light',
      primary: {
        main: primary,
        dark: '#143C8C',
        contrastText: '#fff',
      },
      secondary: {
        main: secondary,
        dark: '#008732',
        contrastText: '#fff',
      },
      error: {
        main: '#BD0043',
        contrastText: '#fff',
      },
      divider: '#D7D6D5',
      background: {
        paper: '#fff',
    }
}
   
  });