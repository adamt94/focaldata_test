# Overview
React web app that lets the user select between surveys and shows the results of the survey to the user.
# Setup
To run the application 
```bash
npm install
npm run start
```
Run Tests
```bash
npm run test
```

# Urls
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

# Packages used
>- Material UI
>- Jest
>- ChartJs





